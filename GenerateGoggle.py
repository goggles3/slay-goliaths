import csv
import os

goggle_name = 'Slay Goliaths'
goggle_description = 'Removes websites owned by "Media Goliaths" from search results'
goggle_public = 'false'
goggle_author = 'anon'

goggle_file = open('goggle.txt', 'w+')

#First, write mandatory metadata

#Write name
goggle_file.write('! name: '+goggle_name+'\n')

#Write description
goggle_file.write('! description: '+goggle_description+'\n')

#Write public status
goggle_file.write('! public: '+goggle_public+'\n')

#Write author
goggle_file.write('! author: '+goggle_author+'\n')

#insert an exra line break between metadata and blocklist. This is just for readability
goggle_file.write('\n')

#collect a list of csv files to use when generating the blocklist
file_names = os.listdir('./Brands')

for file_name in file_names:
    goggle_file.write('! '+'Websites owned by '+file_name[:-4]+'\n')
    
    site_list = open('./Brands/'+file_name)
    reader = csv.reader(site_list)
    for entry in reader:
        goggle_file.write('$discard,site='+entry[1]+'\n')
    site_list.close
    goggle_file.write('\n')

goggle_file.close()